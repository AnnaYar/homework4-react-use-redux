import './CartPage.scss'
import ProductCard from '../components/ProductCard/ProductCard';
import { useOutletContext } from 'react-router-dom';

    
const CartPage = () => {
    const { items, cart, setCart, showModal } = useOutletContext();

    const itemsFiltered = items.filter((product) => {
        return cart.includes(product.id)
    })

    return (
        <div className='container'>
            <h2 className='cart-page__title'>Кошик</h2>
            {itemsFiltered.map((product) =>
                <ProductCard
                        imageUrl ={product.imageUrl}
                        name={product.name}
                        sku={product.sku}
                        color={product.color}
                        price={product.price}
                        key={product.id}
                        id={product.id}
                        setCart={setCart}
                        isCartPage={true}
                        hideAddToCartButton={true}
                        showModal={showModal}
                />
            )
            }
        </div>
    )
}

export default CartPage;