import './CartPage.scss'
import ProductCard from '../components/ProductCard/ProductCard';
import { useOutletContext } from 'react-router-dom';
import './FavoritePage.scss'

    
const FavoritePage = ({ }) => {
    const { items, star, setStar, showModal, addToStar} = useOutletContext();

    const itemsFiltered = items.filter((product) => {
        return star.includes(product.id)
    })

    return (
        <div className='container'>
            <h2 className='favorite-page__title'>Вибране</h2>
            {itemsFiltered.map((product) =>
                <ProductCard
                        imageUrl={product.imageUrl}
                        name={product.name}
                        sku={product.sku}
                        color={product.color}
                        price={product.price}
                        key={product.id}
                        id={product.id}
                        setStar={setStar}
                        isFavoritePage={true}
                        showModal={showModal}
                        hideAddToCartButton={true}
                        addToStar={addToStar} 
                />
            )
            }
        </div>
    )
}

export default FavoritePage;

