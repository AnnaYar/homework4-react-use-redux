import Modal from '../Modal/Modal'
import ModalBody from '../ModalBody/ModalBody' 
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';


const ModalText = ({ firstText, firstClick, isSecondModal, productName, productDescription }) => {
  const dispatch = useDispatch();
  const modal = useSelector((state) => state.modal);

  return (
    <>
      <Modal
        active={modal.active && modal.type === 'text'}
        setActive={() => dispatch({ type: 'close_modal' })}
        firstText={firstText}
        firstClick={firstClick}
        isSecondModal={isSecondModal}
        productName={productName}>
        <ModalBody>
          <h2 className="modal-body__title modal-body__title_magin-second-modal">Add {productName}</h2>
          <p className='modal-body__text modal-body__text_margin-second-modal'>{productDescription}</p>
        </ModalBody>
      </Modal>
    </>
    )
}

ModalText.propTypes = {
    active: PropTypes.bool.isRequired,              
    setActive: PropTypes.func.isRequired,           
    firstText: PropTypes.string,                    
    firstClick: PropTypes.func,                     
    isSecondModal: PropTypes.bool,                  
    productName: PropTypes.string.isRequired,       
    productDescription: PropTypes.string.isRequired 
};

ModalText.defaultProps = {
    firstText: '',              
    firstClick: () => {},       
    isSecondModal: false,       
};

export default ModalText;
