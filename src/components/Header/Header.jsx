import logoImage from '../../assets/sinsay-logo.png'
import cartIcon from '../../assets/cart-icon.png'
import starIcon from '../../assets/star-icon.svg'
import PropTypes from 'prop-types';
import './Header.scss'
import { Link } from "react-router-dom";


const Header = ({ cart, star }) => {

    return (
        <>
            <div>
                <nav className='header-nav container'>
                    <Link to="/"><img src={logoImage} width='150' alt="" /></Link>
                    <div className='header-nav__menu'>
                        <Link to="/woman" className='header-nav__item'>Жінка</Link>
                        <Link to="/man" className='header-nav__item'>Чоловік</Link>
                        <Link to="/child" className='header-nav__item'>Дитина</Link>
                        <Link to="/baby" className='header-nav__item'>Немовля</Link>
                        <Link to="/home" className='header-nav__item'>Дім</Link>
                    </div>
                    <div className='header-nav__icons-box'>
                        <Link to="/favorite" className="star">
                            <img className="star__icon" width='30' src={starIcon} alt="starIcon" />
                            <span>{star.length}</span>
                        </Link>
                        <Link to="/cart" className="cart">
                            <img className="cart__icon" width='30' src={cartIcon} alt="cartIcon" />
                            <span>{cart.length}</span>
                        </Link>
                    </div>
                </nav>
            </div>
            
        </>
    )
}

Header.propTypes = {
    cart: PropTypes.array,  
    star: PropTypes.array,  
};

Header.defaultProps = {
    cart: [],  
    star: [],  
};

export default Header;