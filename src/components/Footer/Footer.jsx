import './Footer.scss'
import facebookIcon from '../../assets/facebook.svg'
import instagramIcon from '../../assets/instagram.svg'
import tiktokIcon from '../../assets/tiktok.svg'
import pinterestIcon from '../../assets/pinterest.svg'
import youtubeIcon from '../../assets/youtube.svg'


const Footer = () => {
    return (
        <>
            <hr />
            <div className='footer'>
                <nav className='footer__nav'>
                    <h4 className='footer__title'>ІНТЕРНЕТ-МАГАЗИН</h4>
                    <ul className='footer__list'>
                        <li className='footer__item'><a href="#">Допомога і з\'язок</a></li>
                        <li className='footer__item'><a href="#">Як здійснити замовлення в інтернет-магазині?</a></li>
                        <li className='footer__item'><a href="#">Мій профіль</a></li>
                        <li className='footer__item'><a href="#">Таблиця розмірів</a></li>
                        <li className='footer__item'><a href="#">Цінова політика</a></li>
                        <li className='footer__item'><a href="#">Шановні покупці</a></li>
                        <li className='footer__item'><a href="#">Купуйте безпечно</a></li>
                        <li className='footer__item'><a href="#">Умови і положення</a></li>
                        <li className='footer__item'><a href="#">Умови проведення акції</a></li>
                        <li className='footer__item'><a href="#">Розсилка новин</a></li>
                    </ul>
                </nav>
                <nav>
                    <h4 className='footer__title'>ДОСТАВКА І ПОВЕРНЕННЯ</h4>
                    <ul className='footer__list'>
                        <li className='footer__item'><a href="#">Способи оплати</a></li>
                        <li className='footer__item'><a href="#">Вартість та час доставки</a></li>
                        <li className='footer__item'><a href="#">Методи повернення</a></li>
                        <li className='footer__item'><a href="#">Як відправити скаргу в інтернет-магазин?</a></li>               
                    </ul>
                </nav>
                <nav>
                    <h4 className='footer__title'>ВІДДІЛ ОБСЛУГОВУВАННЯ КЛІЄНТІВ</h4>
                    <ul className='footer__list'>
                        <li className='footer__item'><a href="#">Про нас</a></li>
                        <li className='footer__item'><a href="#">Мобільний додаток</a></li>
                        <li className='footer__item'><a href="#">Sinsay Club</a></li>
                        <li className='footer__item'><a href="#">Правила програми Sinsay Club</a></li>
                        <li className='footer__item'><a href="#">Сталий розвиток</a></li>
                        <li className='footer__item'><a href="#">Найближчі відкриття магазинів Sinsay</a></li>
                        <li className='footer__item'><a href="#">Адреси магазинів</a></li>
                    </ul>
                </nav>
                <nav>
                    <h4 className='footer__title'>ДІЗНАТИСЯ БІЛЬШЕ</h4>
                    <ul className='footer__sotial-list'>
                        <li className='footer__sotial-icon'><a href="#"><img className='footer__icon' src={facebookIcon} alt="Facebook" /></a></li>
                        <li className='footer__sotial-icon'><a href="#"><img src={instagramIcon} alt="Instagram" /></a></li>
                        <li className='footer__sotial-icon'><a href="#"><img src={tiktokIcon} alt="Tiktok" /></a></li>
                        <li className='footer__sotial-icon'><a href="#"><img src={pinterestIcon} alt="Pinteres" /></a></li> 
                        <li className='footer__sotial-icon'><a href="#"><img src={youtubeIcon} alt="Youtube" /></a></li>   
                    </ul>
                </nav> 
            </div>
        </>
    )
}

export default Footer;