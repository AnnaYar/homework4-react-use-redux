import './ModalWrapper.scss'
import PropTypes from 'prop-types';

const ModalWrapper = ({active, setActive,children}) => {
    return (
        <>
            <div className={active? 'modal-wrapper active':'modal-wrapper'} onClick = {()=>setActive(false)}>
                <div className={active ? 'modal-wrapper__content active' : 'modal-wrapper__content'} onClick={e => e.stopPropagation()}>
                    {children}
                </div> 
            </div>    
        </>
    )
}

ModalWrapper.propTypes = {
    active: PropTypes.bool.isRequired,      
    setActive: PropTypes.func.isRequired,   
    children: PropTypes.node                
};

ModalWrapper.defaultProps = {
    children: null                         
};

export default ModalWrapper;