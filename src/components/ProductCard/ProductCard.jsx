import Button from '../Button/Button';
import './ProductCard.scss'
import StarIcon from '../StarIcon/StarIcon';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';


const ProductCard = (props) => {
    const dispatch = useDispatch();

    const cart = useSelector(state => state.cart.cart);
    const star = useSelector(state => state.star.star);
    
    const isInCart = cart.includes(props.id);
    const isStarred = star.includes(props.id);

    function handleStarClick(event) {
        event.preventDefault();
    if (isStarred) {
        dispatch({ type: 'remove_from_star', payload: props.id });
    } else {
        dispatch({ type: 'add_to_star', payload: props.id });
    }
    };

        return (
            <div className="product">
                <a className={`product__link ${props.isFavoritePage ? 'favorite__link' : ''} ${props.isCartPage ? 'favorite__link' : ''}`} href="#">
                    <div className='product__image-container'>
                        <img className={`product__image ${props.isFavoritePage ? 'favorite__image' : ''} ${props.isCartPage ? 'favorite__image' : ''}`} src={props.imageUrl} alt="" />
                        
                        <div className="product__icon">
                            <StarIcon starClick={isStarred} handleStarClick={handleStarClick} /> 
                        </div>
                    </div>
                    <div className={`${props.isFavoritePage ? 'favorite__info' : ''} ${props.isCartPage ? 'favorite__info' : ''}`}>
                        <div className='product__main-info'>
                        <h3 className="product__name">{props.name}</h3>
                    </div>
                    <div className={`product__description ${props.isFavoritePage ? 'favorite__description' : ''} ${props.isCartPage ? 'favorite__description' : ''}`}>
                        <div>
                            <p className="product__vendor-code">{props.sku}</p>
                            <p className="color">{props.color}</p>
                            <p className="product__price">{props.price} UAH</p>
                        </div>
                        <div>
                            {!props.hideAddToCartButton && (
                                isInCart ?
                                    (
                                    <Button className='btn btn-card' disabled>In cart</Button>
                                    ) : (
                                    <Button onClick={(event) => {
                                    event.preventDefault();
                                    props.showModal('text', props.id);
                                        }} className='btn btn-card'>Add to cart</Button>
                                    )
                                )
                            }
                                
                            {props.isCartPage && (
                                <Button onClick={(event) => {
                                    event.preventDefault();
                                    props.showModal('image', props.id);
                                    }} className='btn btn-card'>&times; Delete </Button>)}
                                
                            {props.isFavoritePage && (
                                isInCart ?
                                    (
                                    <Button className='btn btn-card' disabled>In cart</Button>
                                    ) : (
                                    <Button onClick={(event) => {
                                        event.preventDefault();
                                        props.showModal('text', props.id);
                                            }} className='btn btn-card'>Add to cart</Button>)
                                )
                            }      
                        </div>
                    </div>
                    </div>
                </a>
            </div>
        )
    }

    ProductCard.propTypes = {
    id: PropTypes.string,               
    imageUrl: PropTypes.string,        
    name: PropTypes.string,            
    sku: PropTypes.toString,             
    color: PropTypes.string,           
    price: PropTypes.number,           
    description: PropTypes.string,                
    addToStar: PropTypes.func,         
    showModal: PropTypes.func          
};

ProductCard.defaultProps = {
    description: '',  
};

export default ProductCard;
