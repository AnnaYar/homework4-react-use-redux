import Modal from '../Modal/Modal'
import ModalBody from '../ModalBody/ModalBody'
import './ModalImage.scss'
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';


const ModalImage = ({firstText, secondaryText, firstClick, secondaryClick, productName, productImage}) => {
    const dispatch = useDispatch();
    const modal = useSelector((state) => state.modal);

    return (
        <>
            <Modal
                active={modal.active && modal.type === 'image'}
                setActive={() => dispatch({ type: 'close_modal' })}
                firstText={firstText}
                secondaryText={secondaryText}
                firstClick={firstClick}
                secondaryClick={secondaryClick}
                productName={productName}>
                <ModalBody>
                    <img className='modal-body__img' src={productImage}  alt="" />
                    <h2 className="modal-body__title modal-body__title_magin-first-modal">{productName} Delete!</h2>
                    <p className='modal-body__text modal-body__text_margin-first-modal'>By clicking the “Yes, Delete” button, {productName} will be deleted</p>
                </ModalBody>
            </Modal>
            
        </>
    )
}

ModalImage.propTypes = {
    active: PropTypes.bool.isRequired,            
    setActive: PropTypes.func.isRequired,         
    firstText: PropTypes.string,                 
    secondaryText: PropTypes.string,              
    firstClick: PropTypes.func,                   
    secondaryClick: PropTypes.func,               
};

ModalImage.defaultProps = {
    firstText: '',                               
    secondaryText: '',                           
    firstClick: () => {},                        
    secondaryClick: () => {},                   
};

export default ModalImage;