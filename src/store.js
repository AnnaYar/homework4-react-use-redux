import { configureStore } from "@reduxjs/toolkit";
import { createAsyncThunk } from "@reduxjs/toolkit";

const fetchProducts = async () => {
    const response = await fetch('products.json');
        if (!response.ok) {
            throw new Error('Failed to fetch products');
        }
    return response.json();
};

export const fetchProductsAsync = createAsyncThunk(
    'products/fetchProducts',
    async () => {
        const data = await fetchProducts();
        return data;
    }
);

const initialProductsState = [];

const initialCartState = {
    cart: JSON.parse(localStorage.getItem('cart')) || [],
};

const initialStarState = {
    star: JSON.parse(localStorage.getItem('star')) || [],
};

const initialModalState = {
    active: false,
    type: null,
};

const productsReducer = (state = initialProductsState, action) => {
    switch (action.type) {

        case fetchProductsAsync.fulfilled.type:
        return action.payload;
        default:
            return state;
    }
};

const cartReducer = (state = initialCartState, action) => {
    switch (action.type) {
        
        case 'add_to_cart':
            return {
                ...state,
                cart: [...state.cart, action.payload]
            };

        case 'remove_from_cart':
            return {
                ...state,
                cart: state.cart.filter(itemId => itemId !== action.payload)
            };

        default:
            return state;
    }
}

const starReducer = (state = initialStarState, action) => {
    switch (action.type) {

        case 'add_to_star':
            return {
                ...state,
                star: [...state.star, action.payload]
            };

        case 'remove_from_star':
            return {
                ...state,
                star: state.star.filter(itemId => itemId !== action.payload)
            };

        default:
            return state;
    }
}


const modalReducer = (state = initialModalState, action) => {
    switch (action.type) {
        case 'open_modal':
            return {
                ...state,
                active: true,
                type: action.payload.type,
            };

        case 'close_modal':
            return {
                ...state,
                active: false,
                type: null,
            };

        default:
            return state;
    }
};

const store = configureStore({
    reducer: {
        items: productsReducer,
        cart: cartReducer,
        star: starReducer,
        modal: modalReducer,
    }
    },
);

export default store;

