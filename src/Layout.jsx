import { Outlet } from "react-router-dom";
import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { fetchProductsAsync } from './store.js'
import { useSelector } from "react-redux";
import PropTypes from 'prop-types';
import Header from './components/Header/Header'
import Footer from "./components/Footer/Footer";
import ModalImage from './components/ModalImage/ModalImage'
import ModalText from './components/ModalText/ModalText'


const Layout = ()=> {

    const dispatch = useDispatch();

    const items = useSelector(state => state.items)
    const cart = useSelector(state => state.cart.cart)
    const star = useSelector(state => state.star.star)
    const modal = useSelector(state => state.modal);

    const [currentObj, setCurrentObj] = useState(null)

    useEffect(() => {
        dispatch(fetchProductsAsync())
    }, [])

    useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart));
    localStorage.setItem('star', JSON.stringify(star));
    localStorage.setItem('items', JSON.stringify(items));
    }, [cart, star, items]);
    
    function showModal(type, currentId) {
        dispatch({
            type: 'open_modal',
            payload: {
                type: type,
            },
        });

        const currentProduct = items.find((product) => product.id === currentId);
        setCurrentObj(currentProduct);
    }

    function closeModal() {
            dispatch({
                type: 'close_modal',
            });

            setCurrentObj(null);
    }
    
    function addToCart(product) {
        const {id}=product
            if (!cart.includes(id)) {
                dispatch({
                    type: 'add_to_cart',
                    payload: id,
                })
            }
    }

    function addToStar(id) {
        if (!star.includes(id)) {
            dispatch({
                type: 'add_to_star',
                payload: id,
            })
        }
    }

    function removeFromCart(product) {
        const { id }=product
        dispatch({
            type: 'remove_from_cart',
            payload: id,
        })
    };

    function removeFromFavorite(product) {
        const { id } = product
        dispatch({
            type: 'remove_from_star',
            payload: id
        })
    };

        return (
            <>
                <Header cart={cart} star={star} />
                <Outlet context={{ items, cart, star, showModal, removeFromCart, removeFromFavorite }} /> 
                {modal.type === 'image' && (
                    <ModalImage
                    active={modal.active}
                    setActive={closeModal}
                    firstText={"NO, CANCEL"}
                    secondaryText={'YES, DELETE'}
                    firstClick={closeModal}
                        secondaryClick={() => {
                            removeFromCart(currentObj);
                            closeModal()
                        }}
                        productName={currentObj.name}
                        productImage={currentObj.imageUrl}
                    />
                )}
                {modal.type === 'text' && (
                    <ModalText
                    active={modal.active}
                    setActive={closeModal}
                    firstText={'ADD TO CART'}
                    isSecondModal={true}
                        firstClick={() => {
                            addToCart(currentObj)
                        }}
                    productName={currentObj.name}
                    productDescription={currentObj.description}
                    showModal={showModal}
                    />
                )}
                <Footer/>
            </>
    )
}

Layout.propTypes = {
    items: PropTypes.arrayOf(
    PropTypes.shape({
        id: PropTypes.number.isRequired,
        type: PropTypes.string,
    })
    ),
    cart: PropTypes.arrayOf(PropTypes.number),
    star: PropTypes.arrayOf(PropTypes.number),
};

Layout.defaultProps = {
    items: [],
    cart: [],
    star: [],
};

export default Layout;
