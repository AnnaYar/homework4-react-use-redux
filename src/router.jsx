import { createBrowserRouter } from "react-router-dom";
import Layout from './Layout';
import MainPage from "./pages/MainPage";
import CartPage from "./pages/CartPage";
import ChildPage from "./pages/ChildPage";
import FavoritePage from "./pages/FavoritePage";


export const router = createBrowserRouter([
    {
        path: '/',
        element: <Layout />,
        children: [
            {
                index: true,
                element: <MainPage/>
            },
            {
                path: '/woman',
                element: <div><h2>Жінка</h2></div>
            },
            {
                path: '/man',
                element: <div><h2>Чоловік</h2></div>
            },
            {
                path: '/child',
                element: <ChildPage/>
            },
            {
                path: '/baby',
                element: <div><h2>Немовля</h2></div>
            },
            {
                path: '/home',
                element: <div><h2>Дім</h2></div>
            },
            {
                path: '/cart',
                element:<CartPage/>
            },
            {
                path: '/favorite',
                element: <FavoritePage/>
            }
            
        ]
    }
]);